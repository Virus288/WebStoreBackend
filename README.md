# COMPANY APP API
## This is simple app to manage company data. 

# 1. Description
### This app can manage data like:
- Employees
- Stores
- Products 
- Ordered products
- Users
### This app was created using node with express as router and mongoDB as database. 
### Config function was preconfigured so all what's required is env file with links ( check point 2. )
### This app has routes which allow it to remove data/ update data/ insert data and find data. ( check point 3. )

# 2. How to use.
### Create '.env' file inside root folder with data
```
MongoLocal = ""
JWT = ""
```
### Where MongoLocal should be url to your mongo database.
### "JWT" should be random string with minimal lenght of  40 characters.

### After creating file, use command "yarn start" or "npm start" to run this app. It will run as long as you exit terminal.
### If you want this app to run forever, use command "yarn StartForEver" or "npm run StartForEver". 
### In order to quit from forever command, use "forever list" and kill process with its id number

# 3. Available routes
## Get `/get`
### Route '/get' will let users to fetch data from database. It require body:
```
{
    type: "",
    category: ""
    id: ""
}
```
### In general, it all means: "find type where category = id".
### Type means type of data. For example 'employee' will fetch an employee.
### Category means category of data you want to find. For example "name" with mean: "find type where category = ...".
### ID means value, you want to get. It most cases its used as id. For example: "find type where category = id".

## Post `/add`
### Route '/add' will let users add data. It required body: 
```
{
    type: "",
    data: {}
}
```
### Type means type of data. For example 'employee' will add an employee.
### Even if you forgot to add some data which is necessary, you'll get object "error" back with error 422 saying what you forgot to add
### In order to see, what's required, go to "Database" => "Schemas". Each schema will have information, what's required for data to be saved.

## Delete `/remove`
### Route '/remove' will let users remove data. It required body: 
```
{
    type: "",
    id: ""
}
```
### Type means type of data. For example 'employee' will remove an employee.
### ID. This app is configured to only remove data with ID provided by users. It will prevent other from removing wrong data by mistake.

## Patch `/update`
### Route '/update' will let users update data. It required body: 
```
{
    type: "",
    data: {},
    id: ""
}
```
### Type means type of data. For example 'employee' will add an employee.
### Data is everything that you want to be changed. For example, if you want to change name and salary of employee, data should be "data: { name: "name", salary: salary }"
### ID is id of element that you want to change.

# 4. What's missing.
## 4.1 JWT should have refresh token but refreshing tokens is not implemented yet.
## 4.2 Possibility to remove/update data using other values than id for multiple document changes.
