import { Router } from "express";

import { get, add, remove, update } from "../Database/DBController"

export const adminRouter = Router();

// Get files
adminRouter.get('/get', get);

// Add files
adminRouter.post('/add', add)

// Remove files
adminRouter.delete('/remove', remove)

// Update files
adminRouter.patch('/update', update)

