// External modules
import express from "express";
const mongoose = require('mongoose');
const http = require('http');

// Internal modules
import { adminRouter } from "./Routers/UserRouter";
import { loginRouter } from "./User/LoginRoutes";
import {logger} from "./logger";

// Middleware
import { middlewareApp } from "./Middleware/middleware"
import { verify } from "./Middleware/authMiddleware"

// Environment variables
require('dotenv').config();

const app = express()

// Listen on http
const httpServer = http.createServer(app);

// Connect to mongoDB
mongoose.connect(process.env.MongoPI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true, useFindAndModify: false })
    .then(() => {
        httpServer.listen(5003, () => {
            console.log('App started. Listening on http 5003');
        });
    })
    .catch((err: ErrorEvent) => {
        logger.error({message: "Couldn't connect to database", dbErr: err})
    });

// Middleware
app.use(middlewareApp)

// Not logged routes
app.use(loginRouter)

// Check if is logged or no
app.use(verify)

// Routes
app.use(adminRouter)

// 404
app.get('*', function(req, res){
    console.log("No route?")
    res.status(404).send('It looks like you trying to access data that does not exist.');
});
