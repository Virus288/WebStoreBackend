import express from "express";
const cors = require("cors");
const cookieParser = require('cookie-parser');

export const middlewareApp = express()

middlewareApp.use(express.json());
middlewareApp.use(cookieParser());
middlewareApp.use(cors(
    {
        origin: 'http://localhost:3003',
        credentials: true
    }
));

middlewareApp.use(function(req: express.Request, res: express.Response, next: express.NextFunction) {
    res.header('Content-Type', 'application/json;charset=UTF-8')
    res.header('Access-Control-Allow-Credentials', "true")
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    )
    next()
})
