import express from "express";
const jwt = require("jsonwebtoken");

export const verify = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    let accessToken = req.cookies.JWT

    if (!accessToken){
        return res.status(403).send({error: true, message: "Token invalid"})
    }

    let payload
    try{
        // If payload won't be able to save data, function will auto eject
        payload = jwt.verify(accessToken, process.env.JWT)
        res.locals.id = payload.id.id
        next()
    }
    catch(e){
        next()
    }
}
