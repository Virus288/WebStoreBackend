
// Employees
export interface Employee {
    name: string,
    address: {
        city: string,
        street: string,
        buildingNumber: number,
        apartmentNumber: number,
        postcode: string
    },
    workplace: {
        city: string,
        street: string,
        buildingNumber: number,
        apartmentNumber: number,
        postcode: string
    },
    phoneNumber: string,
    birthday: Date,
    hireDate: Date,
    salary: string
}

// Orders
interface OrderProducts {
    [index: number]: {
        productID: string,
        amount: number
    }

    forEach(param: (product: OrderProducts) => void): void;
}

export interface Order {
    buyerId: string,
    products: OrderProducts,
    completed: boolean
}

// Products
export interface Product {
    name: string,
    price: number,
    type: string,
    amount: number,
    properties: Object
}

// Stores

interface storeEmployee {
    employeeID: string
}

export interface Store {
    address: {
        city: string,
        street: string,
        buildingNumber: number,
        apartmentNumber: number,
        postcode: string
    },
    employees: storeEmployee
}
