import express from "express";

import { Database } from "./Database"

// Get files
export const get = (req: express.Request, res: express.Response) => {
    let Data = new Database(req.body.type, req.body.category, req.body.data, req.body.id)

    Data.find()
        .then((data) => {
            if(data.error === true){
                res.status(422).send(data)
            } else {
                res.status(200).send(data)
            }
        })
}

// Add files
export const add = (req: express.Request, res: express.Response) => {
    if(req.body.data === undefined){
        return res.status(422).send({message: "Some data is missing"})
    }

    let Data = new Database(req.body.type, req.body.category, req.body.data, req.body.id)

    Data.add()
        .then((data) => {
            if(data.error === true){
                return res.status(422).send(data)
            } else {
                return res.status(201).send({message: "Success"})
            }
        })

}

// Remove files
export const remove = (req: express.Request, res: express.Response) => {
    if(req.body.type === undefined){
        return res.status(422).send({message: "'Type' is missing"})
    }

    let Data = new Database(req.body.type, req.body.category, req.body.data, req.body.id)

    Data.remove()
        .then((data) => {
            if(data === null){
                return res.status(422).send({message: "Something is missing. Database returned 'not found'"})
            } else {
                if(data.error === true){
                    return res.status(422).send(data)
                } else {
                    return res.status(201).send({message: "Success"})
                }
            }
        })

    // TODO
    // update działa. Zostaje jedynie remove
}

// Remove files
export const update = (req: express.Request, res: express.Response) => {
    if(Object.keys(req.body.data).length === 0 || req.body.id === undefined){
        return res.status(422).send({message: "Some data is missing"})
    }

    let Data = new Database(req.body.type, req.body.category, req.body.data, req.body.id)

    Data.update()
        .then((data: any) => {
            if(data.error === true){
                return res.status(422).send(data)
            } else {
                return res.status(201).send({message: "Success"})
            }
        })
}
