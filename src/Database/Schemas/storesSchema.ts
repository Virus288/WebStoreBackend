import mongoose from "mongoose"

import { address } from "./adressSchema";

// Stores
export const storeSchema = new mongoose.Schema({
    address : {
        type: address,
        required: [true, 'Please enter store address']
    },
    employees: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'employee',
        required: [false]
    }
},{ timestamps: false });
