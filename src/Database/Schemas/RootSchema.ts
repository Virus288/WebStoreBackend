import mongoose from "mongoose"

// Internal schemas
import { productSchema } from "./productSchema"
import { employeeSchema } from "./employeeSchema";
import { ordersSchema} from "./ordersSchema";
import { storeSchema } from "./storesSchema"

// Employee
export const employee = mongoose.model('employee', employeeSchema);

// Products
export const product = mongoose.model('product', productSchema);

// Orders
export const order = mongoose.model('order', ordersSchema);

// Stores
export const store = mongoose.model('store', storeSchema);
