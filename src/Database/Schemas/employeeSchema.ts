import mongoose from "mongoose"

import { address } from "./adressSchema";

// Employee
export const employeeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please enter employee name'],
        maxLength: 50
    },
    address: {
        type: address,
        required: false
    },
    workplace: {
        type: address,
        required: false
    },
    phoneNumber: {
        type: String,
        required: [true, "Please enter employee phone number"]
    },
    birthday: {
        type: Date,
        required: [true, "Please enter employee birthday"]
    },
    hireDate: {
        type: Date,
        required: [true, "Please enter employee hire date"]
    },
    salary: {
        type: Number,
        required: [true, "Please enter employee salary"]
    }
},{ timestamps: false });
