import mongoose from "mongoose"

// Product
export const productSchema = new mongoose.Schema({
    name : {
        type: String,
        required: [true, 'Please enter an product name']
    },
    price: {
        type: Number,
        required: [true, 'Please enter product price']
    },
    type: {
        type: String,
        required: [true, 'Please enter type of product']
    },
    amount: {
        type: Number,
        required: [true, "Pleas enter available amount"]
    },
    properties: {
        type: Object,
        required: false
    }
},{ timestamps: false });
