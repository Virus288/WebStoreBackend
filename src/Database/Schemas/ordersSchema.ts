import mongoose from "mongoose"

const products = new mongoose.Schema({
    productID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'products',
        required: [true, "Please enter product id"]
    },
    amount: {
        type: Number,
        required: [true, "Please enter amount of products"]
    }
})

export const ordersSchema = new mongoose.Schema({
    buyerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: [true, "Please enter buyer id"]
    },
    products: {
        type: [products],
        required: [true, "Please enter bought products"]
    },
    completed: {
        type: Boolean,
        default: false,
        required: false
    },
},{ timestamps: true });
