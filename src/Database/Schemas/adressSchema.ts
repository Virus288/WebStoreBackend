import mongoose from "mongoose"

// Address
export const address = new mongoose.Schema({
    city : {
        type: String,
        required: false,
        maxLength: 50
    },
    street: {
        type: String,
        required: false,
        maxLength: 50
    },
    buildingNumber: {
        type: Number,
        required: false,
        maxLength: 50
    },
    apartmentNumber: {
        type: Number,
        required: false,
        maxLength: 50
    },
    postcode: {
        type: String,
        required: false,
        maxLength: 50
    }
});
