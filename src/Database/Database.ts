
// Internal modules
import {logger} from "../logger";

// Schemas
import { employee, order, product, store } from "./Schemas/RootSchema"

// Interfaces
import {Employee, Order, Product, Store} from "./Interfaces"

export class Database {
    private readonly id: string | undefined;
    private readonly type: string;
    private readonly data: object;
    private readonly category: string | undefined;
    private errors: { [key: string]: string; } = {};

    constructor(type: string, category: string | undefined, data: object, id: string | undefined){
        // Set up properties
        this.id = id;
        this.type = type;
        this.data = data;
        this.category = category
    }

    // Find item
    find = async () => {
        if(this.type === "employee"){

            if(this.id === undefined){
                return employee.find(function (err: object, data: object) {
                    if(err){
                        logger.error({message: "Database error", dbErr: err})
                        return {error: true, data: err}
                    } else {
                        return data
                    }
                })
            } else {
                if(this.category === undefined){
                    return {error: true, data: "Category not provided"}
                } else {
                    return employee.find({[this.category]: { $in: this.id}}, function (err, data) {
                        if(err){
                            logger.error({message: "Database error", dbErr: err})
                            return {error: true, data: err}
                        } else {
                            return data
                        }
                    })
                }
            }

        } else if(this.type === "order"){

            if(this.id === undefined){
                return order.find(function (err: object, data: object) {
                    if(err){
                        logger.error({message: "Database error", dbErr: err})
                        return {error: true, data: err}
                    } else {
                        return data
                    }
                })
            } else {
                if(this.category === undefined){
                    return {error: true, data: "Category not provided"}
                } else {
                    return order.find({[this.category]: { $in: this.id}}, function (err, data) {
                        if(err){
                            logger.error({message: "Database error", dbErr: err})
                            return {error: true, data: err}
                        } else {
                            return data
                        }
                    })
                }
            }

        } else if(this.type === "product"){

            if(this.id === undefined){
                return product.find(function (err: object, data: object) {
                    if(err){
                        logger.error({message: "Database error", dbErr: err})
                        return {error: true, data: err}
                    } else {
                        return data
                    }
                })
            } else {
                if(this.category === undefined){
                    return {error: true, data: "Category not provided"}
                } else {
                    return product.find({[this.category]: { $in: this.id}}, function (err, data) {
                        if(err){
                            logger.error({message: "Database error", dbErr: err})
                            return {error: true, data: err}
                        } else {
                            return data
                        }
                    })
                }
            }

        } else if(this.type === "store"){

            if(this.id === undefined){
                return store.find(function (err: object, data: object) {
                    if(err){
                        logger.error({message: "Database error", dbErr: err})
                        return {error: true, data: err}
                    } else {
                        return data
                    }
                })
            } else {
                if(this.category === undefined){
                    return {error: true, data: "Category not provided"}
                } else {
                    return store.find({[this.category]: { $in: this.id}}, function (err, data) {
                        if(err){
                            logger.error({message: "Database error", dbErr: err})
                            return {error: true, data: err}
                        } else {
                            return data
                        }
                    })
                }
            }

        } else {
            return {error: true, data: "Unknown type of data"}
        }
    }

    // Add item
    add = async () => {
        let NewItem;

        if(this.type === "employee"){

            const employeeData = <Employee>this.data

            // Check if data exists
            if(!employeeData.name){
                this.addError("name", "Property 'name' is missing")
            }
            if(!employeeData.phoneNumber){
                this.addError("phoneNumber", "Property 'phoneNumber' is missing")
            }
            if(!employeeData.birthday){
                this.addError("birthday", "Property 'birthday' is missing")
            }
            if(!employeeData.hireDate){
                this.addError("hireDate", "Property 'hireDate' is missing")
            }
            if(!employeeData.salary){
                this.addError("salary", "Property 'salary' is missing")
            }

            if(Object.keys(this.errors).length > 0){
                return {error: true, err: this.errors}
            }

            NewItem = new employee(employeeData)

        } else if(this.type === "order"){

            const orderData = <Order>this.data

            // Check if data exists
            if(!orderData.buyerId){
                this.addError("buyerId", "Property 'buyerId' is missing")
            }
            if(!orderData.products){
                this.addError("products", "Property 'products' is missing")
            } else {
                for(let x=0; x < Object.keys(orderData.products).length; x++){
                    if(orderData.products[x].productID === undefined || orderData.products[x].amount === undefined){
                        this.addError("product", "Some data is missing inside of 'product' object")
                    }
                }
            }
            if(!orderData.completed){
                orderData.completed = false
            }

            if(Object.keys(this.errors).length > 0){
                return {error: true, err: this.errors}
            }

            NewItem = new order(orderData)

        } else if(this.type === "product"){

            const productsData = <Product>this.data

            // Check if data exists
            if(!productsData.name){
                this.addError("name", "Property 'name' is missing")
            }
            if(!productsData.price){
                this.addError("price", "Property 'price' is missing")
            }
            if(!productsData.type){
                this.addError("type", "Property 'type' is missing")
            }
            if(!productsData.amount){
                this.addError("amount", "Property 'amount' is missing")
            }
            if(!productsData.properties){
                productsData.properties = {}
            }

            if(Object.keys(this.errors).length > 0){
                return {error: true, err: this.errors}
            }

            NewItem = new product(productsData)

        } else if(this.type === "store"){

            const storeData = <Store>this.data

            // Check if data exists
            if(!storeData.address){
                this.addError("address", "Property 'address' is missing")
            } else {
                if(!storeData.address.city){
                    this.addError("city", "Property 'city' is missing inside of 'address' object")
                }
                if(!storeData.address.street){
                    this.addError("street", "Property 'street' is missing inside of 'address' object")
                }
                if(!storeData.address.buildingNumber){
                    this.addError("buildingNumber", "Property 'buildingNumber' is missing inside of 'address' object")
                }
                if(!storeData.address.postcode){
                    this.addError("postcode", "Property 'postcode' is missing inside of 'address' object")
                }
            }
            if(!storeData.employees){
                this.addError("employees", "Property 'employees' is missing")
            }

            if(Object.keys(this.errors).length > 0){
                return {error: true, err: this.errors}
            }

            NewItem = new store(storeData)
            console.log(NewItem)

        } else {
            return {error: true, data: "Unknown type of data"}
        }

        try {
            await NewItem.save()
            return NewItem
        } catch (err) {
            logger.error({message: "Database error", dbErr: err})
            return {error: true, data: err}
        }
    }

    // Update item
    update = async () => {

        if(this.type === "employee"){
            try {
                return await employee.findOneAndUpdate({_id: this.id}, {...this.data})
            } catch (err) {
                logger.error({message: "Database error", dbErr: err})
                return {error: true, data: err}
            }

        } else if(this.type === "order"){

            try {
                return await order.findOneAndUpdate({_id: this.id}, {...this.data})
            } catch (err) {
                logger.error({message: "Database error", dbErr: err})
                return {error: true, data: err}
            }

        } else if(this.type === "product"){

            try {
                return await product.findOneAndUpdate({_id: this.id}, {...this.data})
            } catch (err) {
                logger.error({message: "Database error", dbErr: err})
                console.log(err)
                return {error: true, data: err}
            }

        } else if(this.type === "store"){

            try {
                return await store.findOneAndUpdate({_id: this.id}, {...this.data})
            } catch (err) {
                logger.error({message: "Database error", dbErr: err})
                return {error: true, data: err}
            }

        } else {
            return {error: true, data: "Unknown type of data"}
        }
    }

    // Remove item
    remove = async () => {

        if(this.type === "employee"){
            try {
                return await employee.findOneAndRemove({_id: this.id})
            } catch (err) {
                logger.error({message: "Database error", dbErr: err})
                return {error: true, data: err}
            }

        } else if(this.type === "order"){

            try {
                return await order.findOneAndRemove({_id: this.id})
            } catch (err) {
                console.log(err)
                logger.error({message: "Database error", dbErr: err})
                return {error: true, data: err}
            }

        } else if(this.type === "product"){

            try {
                return await product.findOneAndRemove({_id: this.id})
            } catch (err) {
                logger.error({message: "Database error", dbErr: err})
                console.log(err)
                return {error: true, data: err}
            }

        } else if(this.type === "store"){

            try {
                return await store.findOneAndRemove({_id: this.id})
            } catch (err) {
                logger.error({message: "Database error", dbErr: err})
                return {error: true, data: err}
            }

        } else {
            return {error: true, data: "Unknown type of data"}
        }

    }

    addError(key: string, val: string){
        this.errors[key] = val;
    }

}
