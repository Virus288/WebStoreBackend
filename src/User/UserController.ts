import express from "express";

const jwt = require("jsonwebtoken")
const {User} = require("./User");

interface userData {
    Type: number;
    Message: string;
    Id: string;
    Role: string,
    error: boolean,
    err: object
}

// Register
module.exports.register = (req: express.Request, res: express.Response) => {
    let Data = new User(req.body.username, req.body.email, req.body.password, req.body.password2);

    Data.ValRegister()
        .then((data: userData) => {
            if(data.error){
                res.status(422).send(data)
            } else {
                res.status(200).send(data)
            }
        res.send(data)
    })
}

// Login
module.exports.login = async (req: express.Request, res: express.Response) => {
    let Data = new User("", req.body.email, req.body.password);

    Data.ValLogin().then((data: userData) => {
        if(!data.Type){
            res.send(data)
        } else {
            let token = createToken({id: data.Id});
            res.cookie("JWT", token, {httpOnly: true, maxAge: maxAge * 1000});
            // Cookie for https only ( lock out postman and 3'rd party apps )
            // res.cookie("JWT", token, {httpOnly: true, maxAge: maxAge * 1000, Secure: true});
            res.send({Type: data.Type, Message: data.Message})
        }
    })
}

module.exports.logout = async (req: express.Request, res: express.Response) => {
    res.cookie("JWT", "Logout", {httpOnly: true, maxAge: 0});
    res.send({Type: 1, Message: "Success"})
}

// Token
const maxAge = 12 * 60 * 60;

const createToken = (id: object) => {
    return jwt.sign({id}, process.env.JWT, {
        expiresIn: maxAge
    })
}
