const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    name : {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: [true, 'Please pass valid email'],
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        required: [true, 'Password is invalid'],
        minlength: [6, "Minimal length is 6 signs"]
    },
    verified: {
        type: Boolean,
        required: true,
        default: 'false'
    },
});

export const User = mongoose.model('user', userSchema);
