const { Router } = require('express');
const { register, login, logout } = require('./UserController');

export const loginRouter = Router();

loginRouter.post('/register', register);
loginRouter.post('/login', login);
loginRouter.get('/logout', logout);
