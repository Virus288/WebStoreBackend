import validator from "validator";
import PasswordValidator from "password-validator"
import bcrypt from "bcrypt"

// Internal modules
import { User as UserSchema } from "./UserSchema"
import { logger } from "../logger";

interface userData {
    id: string;
    name: string;
    email: string;
    password: string;
    verified: string;
}

class User {
    private readonly username: string;
    private readonly email: string;
    private readonly password: string;
    private readonly password2: string;
    private errors: { [key: string]: string; } = {};

    constructor(username: string, email: string, password: string, password2: string = ""){
        // Set up properties
        this.username = username;
        this.email = email;
        this.password = password;
        this.password2 = password2;
    }

    async ValRegister() {
        await this.CheckUser()
            .then((data: userData[]) => {
                if(data.length > 0){
                    this.addError("email", "Email is already registered")
                }
            })
        if(Object.keys(this.errors).length > 0){
            return {error: true, err: this.errors}
        } else {
            this.ValEmail();
            this.ValUsername();
            this.ValPass();

            if (Object.keys(this.errors).length === 0) {
                await this.register()

                if(Object.keys(this.errors).length > 0){
                    return {error: true, err: this.errors}
                } else {
                    return {Type: 1, Message: "Success"}
                }

            } else {
                return this.errors
            }

        }
    }

    async ValLogin() {
        if(!this.password){
            this.addError("password", "Invalid password")
        }
        if(!this.email){
            this.addError("email", "Invalid email")
            return this.errors
        }
        await this.ValEmail();
        if(Object.keys(this.errors).length > 0){
            return {error: true, err: this.errors}
        } else {
            return await this.CheckUser()
                .then(async (data: userData[]) => {
                    if(Object.keys(this.errors).length > 0){
                        return {error: true, err: this.errors}
                    } else {
                        if(data.length > 0){
                            return await this.login(data[0].password, data[0].id)
                        } else {
                            this.addError("email", "Email is not registered")
                            return {error: true, err: this.errors}
                        }
                    }
                })
        }
    }

    // Check name
    ValUsername(){
        if(!this.username || this.username.length <= 4){
            this.addError("username", "Username should contain at least 4 characters")
        }
    }

    ValEmail(){
        if(!validator.isEmail(this.email)){
            this.addError("email", "Invalid email")
        }
    }

    ValPass(){
        if(this.password2.length < 6){
            this.addError("password2", "Repeated password should be at least 6 characters long")
        } else if(this.password !== this.password2){
            this.addError("password", "Passwords are not equal")
        } else {
            let schema = new PasswordValidator();

            schema
                .is().min(6)
                .is().max(100)
                .has().uppercase()
                .has().lowercase()
                .has().digits(1)
                .has().not().spaces();

            if(!schema.validate(this.password)){
                this.addError("password", "Invalid password. Password should contain at least 6 characters, upper case and lower case letters and at least 1 number")
            }
        }
    }

    CheckUser = async () => {
        return UserSchema.find({email: this.email}, (err: any, user: object) => {
            if (err) {
                this.addError("username", err)
            } else {
                logger.error({message: "Database error", dbErr: err, body: {username: this.username, email: this.email}})
                return user
            }
        });
    }

    async login(password: string, id: string) {
        const auth = await bcrypt.compare(this.password, password)
        if (auth) {
            return {Type: 1, Message: "Success", Id: id}
        } else {
            this.addError("password", "Invalid password")
            return {error: true, err: this.errors}
        }
    }

    register = async () => {
        const salt = await bcrypt.genSalt();
        let hashedPassword: string;
        hashedPassword = await bcrypt.hash(this.password, salt)

        let NewUser = new UserSchema({name: this.username, email: this.email, password: hashedPassword, role: "user"});

        try {
            await NewUser.save()
            return NewUser
        } catch (err) {
            logger.error({message: "Database error", body: {username: this.username, email: this.email}, dbErr: err})
            this.addError("user", err)
        }

    }

    addError(key: string, val: string){
        this.errors[key] = val;
    }

}

module.exports = {
    User
}
